#!/bin/bash

ARCHIVE="archive/"

mkdir -p $ARCHIVE $ARCHIVE/chateau
cd chateau
python script.py
cd ..
cp chateau/rendu/*.pdf $ARCHIVE/chateau
make -C doc
cp doc/fiche.pdf doc/guide.pdf doc/drapeaux.pdf $ARCHIVE
make -C manuel
cp manuel/manuel_{orange,vert}.pdf $ARCHIVE
make -C protocole
cp protocole/protocole.pdf $ARCHIVE

tar czvf CSU.tar.gz $ARCHIVE
