\documentclass{article}
\usepackage{../style}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{changepage}
\usepackage{subcaption}

\input{enigme}

\newcommand{\timee}[1]{{\normalfont\large #1 minutes}}
\newcommand{\chateau}[3]{
  \begin{figure}[!ht]
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=.9\textwidth]{../chateau/images/vert_#1.png}
      \caption{Chateau Vert -- Etage #1}
      \begin{adjustwidth}{1cm}{1cm}
        \it \centering #2
      \end{adjustwidth}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=.9\textwidth]{../chateau/images/orange_#1.png}
      \caption{Chateau Orange -- Etage #1}
      \begin{adjustwidth}{1cm}{1cm}
        \it \centering #3
      \end{adjustwidth}
    \end{subfigure}
  \end{figure}
}

\definecolor{Gray}{gray}{0.85}

\title{Pris au piège dans un chateau \\ Guide de l'activité}
\author{Victor Alfieri et Pierre-André Crepon}
\date{}


\begin{document}
\maketitle
\pagenumbering{gobble}

\section{Le public}

Cette activité s'adresse à des enfants en fin de primaire ou en début de
collège. Elle utilise des bases de calcul assez simples : addition,
soustraction et multiplication. De plus, des notions élémentaires
d'algorithmique sont abordées : disjonction de cas, conditions, séquence.

\section{Le contexte -- \timee{5}}
Le principe est le suivant : deux aventurier·ère·s sont bloqué·e·s dans deux
chateaux différents, dont iels cherchent à pénétrer le cœur pour y trouver les
ingrédients d'un antidote qui sauvera le roi empoisonné. Chaque chateau est
composé de plusieurs étages, et il faut résoudre toutes les énigmes d'un étage
pour pouvoir descendre au suivant.

Cependant, la solution des énigmes d'un chateau se trouve dans l'autre chateau.
Ainsi, chaque aventurier·e doit communiquer à l'autre les mécanismes qu'iel voit
dans son propre chateau pour que l'autre puisse les résoudre, puis à son tour
résoudre les énigmes de son·sa camarade. Pour se parler, iels ne peuvent que se
faire des signes avec des drapeaux, et, connaissant à l'avance les différentes
salles que pourraient comporter les chateaux, iels ont donc mis au point un
langage adapté.

Les élèves travaillent par groupe de six (ou quatre), une paire de trinômes (ou de binômes), et chaque
trinôme dispose d'un chateau, comportant plusieurs étages. Physiquement, un étage
est une feuille A4 sur laquelle est imprimée un plan de l'étage avec plusieurs
salles. Le chateau entier comporte trois étages. Iels commenceront par résoudre
des problèmes simples qui se complexifieront au fur et à mesure. Pour vérifier
qu'iels ont bien résolu les énigmes de leurs étages, les élèves disposent d'un
cadenas dans lequel il faut rentrer un code, code qu'iels détermineront grâce à
la solution des énigmes de l'étage.

L'activité n'est évidemment pas une évaluation, elle ne doit pas être stressante
ni frustrante pour les élèves, il ne doit pas y avoir de pression pour aller
au bout. Il est intéressant d'aborder l'étage 3, sans chercher à le terminer.

\section{Matériel nécessaire \& Travail à faire en amont}

\begin{itemize}
  \item Imprimer, pour chaque groupe de 6 (ou 4 selon la taille de la classe) élèves :
    \begin{itemize}
      \item Deux sets de drapeaux, c'est le fichier "drapeaux", il faudra y découper les différents drapeaux
      \item Deux protocoles de communication, c'est le fichier "protocole"
      \item Un exemplaire de chacun des manuels (vert et orange), ce sont les
        deux fichiers "manuel\_orange" et "manuel\_vert"
      \item Un exemplaire de chacun des étages des deux chateaux, ce sont tous les fichiers du dossier "chateau"
      \item Enfin, chaque élève devra disposer d'un peu de brouillon pour résoudre les énigmes
    \end{itemize}
  \item Prévoir pour chacun des groupes 6 cadenas à 3 chiffres, et pour chacun
    d'entre eux prévoir de les étiquetter et d'y rentrer un code :
    \begin{itemize}
      \item Cadenas "V1" : 216
      \item Cadenas "V2" : 240
      \item Cadenas "V3" : 373
      \item Cadenas "O1" : 123
      \item Cadenas "O2" : 321
      \item Cadenas "O3" : 555
    \end{itemize}
\end{itemize}


\section{L'exemple d'un premier chateau -- \timee{15}}

Pour introduire le fonctionnement de l'activité, commençons par un exemple. On
présentera également cet exemple aux élèves au tableau.
Le chateau présenté en figure \ref{fig:violet} est un premier exemple de chateau
que pourraient être amenés à résoudre nos deux aventurier·ère·s. Pour se faire,
iels devront commencer par communiquer ce qu'iels voient à l'interieur du
chateau. Pour cela, on utilise un protocole de communication, qui a pour but de
permettre de décrire le contenu des salles mais aussi la manière de transmettre
la résolution des énigmes qui s'y trouvent.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{../chateau/images/violet_1.png}
  \caption{Un premier chateau}
  \label{fig:violet}
\end{figure}

Les drapeaux sont les bases de la communication entre les élèves, ce sont comme les
lettres d'un alphabet. Iels en disposent de sept, tous marqués de symboles
différents : \rond, \porte, \los, \croix, \etoile, \carre\ et \tri. Vous pouvez
pour la séquence qui va suivre les distribuer aux élèves.

\subsection{Décrire les énigmes -- \timee{4}} \label{protocole}
Vous pouvez maintenant décrire au tableau les parties suivantes du protocole de
communication. La structure d'un message est la suivante : pour chaque salle de l'étage, on
commence par déclarer le symbole de la salle, puis on décrit son contenu.
Dans le protocole, il y a aussi une partie décrivant les actions à effectuer dans la salle, mais cela
ne servira qu'au moment où il faudra résoudre une énigme, pour le moment on cherche
simplement à la décrire. La manière dont on décrit ce qu'on voit dépend de la salle : \\

\noindent\fbox{
  \begin{minipage}{\textwidth}
    \subsubsection*{Salle \rond}
    C'est une salle où il n'y a qu'une statue dont le casque est marqué d'un
    symbole. Il faudra ou bien enlever le casque de la statue ou bien le laisser.

    Description de la salle : montrez le même symbole que celui sur le casque.

    Action dans la salle : montrez un \tri\ pour enlever le casque ou bien une
    \croix\ pour le laisser en place.

    \subsubsection*{Salle \porte}
    A la fin d'un étage, il peut y avoir deux portes de sorties au lieu d'une.
    Attention, derrière l'une des portes se trouve un dragon. Chacune est marquée
    d'un symbole.

    Description de la salle : montrez les symboles sur les portes, dans n'importe
    quel ordre.

    Action dans la salle : montrez le symbole de la bonne porte.
  \end{minipage}
} \\

\noindent Vous pouvez demander aux élèves de décrire les salles du chateau
violet avec les drapeaux que vous avez distribués. Il est important que les élèves notent le message quelque part, il faudra s'en resservir à la dernière étape.

Voici la solution. Le message à envoyer pour décrire le chateau est le suivant :
\rond\los\porte\los\croix\ (\porte\los\croix\rond\los\ fonctionne
également).

En effet, voici comment ce message est élaboré :

\begin{itemize}
  \item[\rond] Ce symbole décrit le fait qu'il y a une salle avec une statue dedans
  \item[\los] Il y a un losange sur le casque de la statue

  \item[\porte] Il y a deux portes vers le prochain étage
  \item[\los] La première est marqué d'un losange
  \item[\croix] La seconde présente une croix
\end{itemize}

\par\noindent Une fois que c'est fait, vous pouvez remettre aux élèves les livrets du protocole de
communication au complet.

\subsection{Résoudre les énigmes -- \timee{4}}

Une fois la description du chateau reçue et la sienne envoyée, l'aventurier.ère
va essayer de résoudre les énigmes qui lui ont été envoyées. Pour l'heure il s'agit
juste de trouver au brouillon ce qu'il va falloir faire, il ne s'agit pas encore de
le transmettre à son ou sa camarade. Pour savoir quoi faire, les élèves
disposent d'un manuel de résolution des énigmes. Chaque énigme se résout de
manière différente. Les premières qui sont présentées sont simples à
résoudre et les suivantes seront plus complexes. \textbf{Attention, la manière
de résoudre une énigme dépend de la couleur du chateau : on ne résoud pas les
chateaux verts, oranges, violets de la même manière.} Nous allons décrire
comment résoudre les énigmes présentes dans le chateau violet :\\

\noindent\fbox{
  \begin{minipage}{\textwidth}
    \subsubsection*{Salle \rond}
    C'est une salle où il n'y a qu'une statue dont le casque est marqué d'un
    symbole. Il faudra ou bien enlever le casque de la statue ou bien le laisser.

    \begin{center}
      \begin{tabular}{|c|c|c|c|c|c|c|c|}
        \hline\rowcolor{Gray}
        \rond & \los & \croix & \etoile & \porte & \tri & \carre \\
        \hline
        Non & Oui & Non & Oui & Oui & Oui & Non \\
        \hline
      \end{tabular}
    \end{center}

    \subsubsection*{Salle \porte}
    À la fin d'un étage, il peut y avoir deux portes de sorties au lieu d'une.
    Attention, une seule est la bonne. Chacune est marquée d'un symbole.

    Choisir le symbole qui correspond à celui sur le casque de la statue.
  \end{minipage}
} \\

\noindent Vous pouvez demander aux élèves de résoudre les énigmes du chateau
violet.

Voici la solution. En consultant le manuel, on voit que pour résoudre l'énigme
rond, il faudra enlever le casque de la statue et pour l'énigme des doubles
portes, il faudra emprunter la porte marqué d'un losange.

\subsection{Envoyer la solution des énigmes -- \timee{4}}

Une fois la solution trouvée, vous pouvez demander aux élèves comment ils la
communiqueraient avec les drapeaux en s'appuyant sur le protocole de communication,
comme décrit à la section \ref{protocole} de ce document : c'est maintenant qu'on consulte
la section du protocole décrivant les actions à effectuer dans la salle, qui indique quoi faire.

Voici la solution. Le message à renvoyer est : \rond\tri\porte\los
(\porte\los\rond\tri marche aussi).

\begin{itemize}
  \item[\rond] on va donner la solution pour l'énigme rond
  \item[\tri] il faut enlever le casque

  \item[\porte] on va donner la solution à l'énigme des doubles portes
  \item[\los] il faudra emprunter la porte marquée d'un losange
\end{itemize}

À nouveau, il est important que les élèves notent le message quelque part, il faudra s'en resservir à la dernière étape, qui est la suivante.


\subsection{Code pour passer à l'étage suivant -- \timee{3}}

Une fois que l'aventurier.ère a trouvé la solution des énigmes et l'a renvoyée à
son ou sa comparse, il ne reste plus qu'à passer à l'étage suivant. Mais avant ça,
pour vérifier qu'iels ne se sont pas trompé.e.s dans la résolution des pièges, ils
devront trouver le code du cadenas qui scelle la porte menant à l'étage suivant.
Pour cela, trois petites devinettes en lien avec la résolution des énigmes
qu'ils viennent de faire permettront de deviner les trois chiffres de la
combinaison. \\

\noindent Voici l'énigme qui est proposée pour le chateau violet :

\begin{adjustwidth}{1cm}{1cm}
  \it Mon premier est le nombre de losanges dans le message que tu as envoyé. Mon
  second est le nombre de ronds dans le message que tu as reçu. Mon dernier
  vaudra 7 si tu as choisi la porte marquée d'un losange et 8 si tu as choisi
  celle marquée d'une croix.
\end{adjustwidth}


Vous pouvez demander aux élèves de trouver le code pour passer à l'étage
suivant. Vous pouvez au besoin leur rappeller les messages qui ont été envoyé
précedement, mais en principe iels ont dû les noter quelque part avant comme demandé : on a envoyé \rond\los\porte\los\croix\ et reçu \rond\tri\porte\los. \\

\noindent Voici la solution. Le code à déchiffrer est donc \textbf{217} : il y a
2 losanges dans le message à envoyer ; 1 rond dans le message reçu et on a
emprunté la porte présentant un losange donc le dernier chiffre est 7.


\section{Déroulement de l'actvité -- \timee{40}}

Pour le reste de l'activité, vous pouvez diviser la classe en groupe de 4 ou 6 élèves (suivant la taille de la classe)
et associer les groupes deux par deux. Assignez à chacun de ces deux groupes la
couleur verte ou orange.

\begin{itemize}
  \item Distribuez aux élèves des groupes oranges les manuels oranges et aux
    élèves des groupes verts les manuels verts.
  \item Distribuez aux élèves des groupes oranges les chateaux verts et aux
    élèves des groupes verts les chateaux oranges (puisque les solutions d'un chateau se trouvent dans l'autre chateau), ainsi que les cadenas
    respectifs pour chaque chateau (par exemple le cadenas étiqueté « v1 » pour le premier étage des
    chateaux verts, ou bien celui étiqueté « o2 » pour le deuxième étage du chateau orange).
\end{itemize}

Ainsi, par exemple les élèves du groupe vert jouent dans le chateau orange. À l'aide du protocole, iels décrivent au groupe orange l'étage, comme à la première étape, et n'oublient pas de noter au brouillon le message qu'iels vont envoyer.
À l'aide du manuel orange, et de la description de l'étage orange que vient de lui faire le groupe vert, le groupe orange résout au brouillon les énigmes de cet étage, comme à la seconde étape.
Une fois résolues, les solutions des énigmes sont envoyées par le groupe orange au groupe vert, qui va noter ce message, c'est la troisème étape.
Enfin, le groupe vert se sert du message envoyé (à l'étape 1) et du message reçu (à l'étape 3) pour trouver le code de son cadenas et passer à l'étage suivant, c'est la dernière étape.
À chaque étape, il est recommandé que les deux groupes travaillent en même temps, il ne s'agit pas d'attendre qu'un groupe ait fini son étage pour dire à l'autre de faire le sien, afin de gagner du temps et de faire réfléchir tous les élèves en même temps.
On répète ces étapes à chaque étage, pour rappel il y a trois étages.

Dans la suite, on donne les solutions à tous les étages des chateaux (en plus de
les commenter rapidement), n'hésitez pas à les consulter pour débloquer les
élèves si besoin. C'est désormais aux élèves de travailler.

\subsection*{Étage 1 -- \timee{5}}

Cet étage est très similaire au premier qui a été résolu au tableau.  Laissez
quelques minutes aux élèves pour chercher comment décrire les premières énigmes.
S'iels n'y arrivent pas, allez les aider au bout de 5 minutes.

\chateau{1}{\cvun}{\coun}

\begin{center}
  \begin{tabular}{|l|c|c|}
    \hline & Chateau Vert & Chateau Orange \\ \hline
    Description de l'énigme & \rond\carre\ \porte\etoile\rond& \rond\etoile\ \porte\rond\croix \\ \hline
    Solution de l'énigme & \rond\tri\ \porte\etoile & \rond\tri\ \porte\rond \\ \hline
    Code du cadenas & 216 & 123\\ \hline
  \end{tabular}
\end{center}
Les groupes de symboles separés par des espaces peuvent être intervertis.

\subsection*{Étage 2 -- \timee{10}}

Dans cet étage, il y a deux nouvelles énigmes. Laissez les élèves se
familiariser avec en utilisant uniquement le protocole de communication. Il se peut
que les élèves ne parviennent pas à comprendre ces nouvelles énigmes en ne s'aidant que des manuels, dans ce cas
n'hésitez pas à les aider au bout de quelques petites minutes car là n'est pas le cœur de l'activité.

\chateau{2}{\cvdeux}{\codeux}

\begin{center}
  \begin{tabular}{|l|c|c|}
    \hline & Chateau Vert & Chateau Orange \\ \hline
    Description de l'énigme &
    \rond\etoile\ \los\etoile\croix\etoile\ \croix\carre\etoile\rond\porte\ \porte\carre\croix &
    \rond\porte\ \los\etoile\etoile\croix\ \croix\los\croix\tri\porte\ \porte\croix\carre\\ \hline
    Solution de l'énigme &
    \rond\croix\ \los\porte\porte\ \croix\porte\etoile\ \porte\carre &
    \rond\etoile\ \los\porte\porte\ \croix\los\tri\ \porte\croix \\ \hline
    Code du cadenas & 240 & 321 \\ \hline
  \end{tabular}
\end{center}

\subsection*{Étage 3 -- \timee{15}}

C'est le dernier étage. Il est volontairement difficile. Il serait plutôt normal que les élèves n'arrivent pas
au bout. En revanche il est important qu'iels s'y essaient toutes et tous pour ressentir la difficulté de la
communication avec les drapeaux, et qu'iels se demandent comment y parvenir à tous les coups.

\chateau{3}{\cvtrois}{\cotrois}

\begin{center}
  \begin{tabular}{|l|c|c|}
    \hline & Chateau Vert & Chateau Orange \\ \hline
    Description de l'énigme &
    \rond\porte\ \los\croix\croix\etoile\ \croix\tri\croix\los\rond\ \etoile\los\rond\porte\croix\ \porte\los\tri &
    \rond\los\ \los\croix\croix\etoile\ \croix\carre\rond\etoile\los\ \etoile\los\rond\porte\croix\ \porte\los\tri\\ \hline
    Solution de l'énigme &
    \etoile\los\tri\tri\los\los\tri\los\los\ \rond\tri\ \los\porte\porte\porte\ \croix\croix\los\ \porte\tri &
    \etoile\los\los\tri\los\tri\los\tri\tri\tri\ \rond\croix\ \los\porte\porte\porte\ \croix\etoile\los\ \porte\los\\ \hline
    Code du cadenas & 373 & 555 \\ \hline
  \end{tabular}
\end{center}

\section{Notions d'informatique abordées -- \timee{5}}

Pour terminer, il faut faire une petite synthèse sur cette activité, pour 
expliquer quel est son intérêt, ce qu'elle enseigne vraiment. C'est pourquoi il
faut arrêter tous les groupes quelques minutes avant la fin.

Pour trouver la solution des énigmes, on doit suivre une suite d'instructions,
parfois avec des conditions ou en respectant une certaine procédure : c'est une
façon très simple de faire l'expérience d'un \textit{algorithme}, c'est-à-dire
une suite d'étapes à appliquer dans un certain ordre pour résoudre un problème.

Lorsqu'un ou une élève reçoit le message codé de son ou sa camarade, il y a au
début une ambiguïté à lever : quels symboles se rapportent à quelles
salles ? Dans le système qu'on propose, on ne peut jamais dire quel est le sens
d'un symbole tout seul : son sens dépend de ceux qui sont autour de lui. Ce qui
permet aux élèves de déterminer le sens d'un message c'est le fait que les
règles du jeu ne permettent pas à n'importe quelle suite de symboles de vouloir
dire quelque chose : un message valide doit respecter un certains nombre de
règles connues à l'avance, dictées dans le protocole. L'ensemble des messages
valides est appelé "\textit{langage}" en informatique, et l'ensemble des règles
qui dictent quels messages font bien partie de ce langage est appelé
"grammaire".  Comme pour les vraies langues naturelles, il y a des grammaires
plus ou moins compliquées. Par exemple, s'il y a plusieurs façons de construire
un même message en utilisant différentes règles de la \textit{grammaire}, alors
on ne peut plus déterminer quelles règles ont été utilisées pour construire le
message une fois reçu, et on dit que la grammaire est "\textit{ambiguë}". Or,
les règles utilisées peuvent avoir du sens, comme c'est le cas dans notre
activité où elles permettent de comprendre de quelle salle on parle.
Heureusement, la grammaire utilisée dans cette activité n'est pas ambiguë. Dans
ce cas, il y a un algorithme très simple pour déterminer comment a été construit
(et donc ce que signifie) un message : il suffit de trouver l'unique manière de
reproduire le même message en utilisant les règles de la grammaire, il suffit
donc en quelque sorte renverser ces règles et les lire à l'envers. Le fait de
partir d'un mot et d'en trouver la signification s'appelle l'\textit{analyse
syntaxique}.

\end{document}


% \subsubsection*{Salle \rond}
% C'est une salle où il n'y a qu'une statue dont le casque est marqué d'un
% symbole. Il faudra ou bien enlever le casque de la statue ou bien le laisser.
% 
% \begin{multicols}{1}
%   \begin{center}
%     Extrait du manuel vert :
%     \begin{tabular}{|c|c|c|c|c|c|c|c|}
%       \hline\rowcolor{Gray}
%       \rond & \los & \croix & \etoile & \porte & \tri & \carre \\
%       \hline
%       Non & Non & Non & Oui & Oui & Non & Oui \\
%       \hline
%     \end{tabular}
%   \end{center}
% \columnbreak
%   \begin{center}
%     Extrait du manuel orange :
%     \begin{tabular}{|c|c|c|c|c|c|c|c|}
%       \hline\rowcolor{Gray}
%       \rond & \los & \croix & \etoile & \porte & \tri & \carre \\
%       \hline
%       Oui & Non & Oui & Non & Oui & Non & Oui \\
%       \hline
%     \end{tabular}
%   \end{center}
% \end{multicols}
% 
% \subsection*{Salle \porte}
% A la fin d'un étage, il peut y avoir deux portes de sorties au lieu d'une.
% Attention, derrière l'une de ces portes se trouve un dragon. Chacune est marquée
% d'un symbole.
% 
% \begin{multicols}{2}
%   \begin{center}
%     Extrait du manuel vert : \\
%     Choisir le symbole qui correspond à une énigme présente dans l'étage.
%   \end{center}
% \columnbreak
%   \begin{center}
%     Extrait du manuel orange : \\
%     Choisir le symbole qui ne correspond pas à une énigme présente dans l'étage.
%   \end{center}
% \end{multicols}
% 
% \subsection*{Salle \los}
% C'est une salle avec trois leviers alignés de gauche à droite. Chacun est déjà
% ou bien levé ou bien baissé au départ. Il faudra changer la position d'un seul
% d'entre eux.
% 
% \begin{multicols}{2}
%   \begin{center}
%     Extrait du manuel vert : \\ \vspace{0.5cm}
%     \begin{enumerate}
%       \item Si le premier et le dernier leviers sont dans la même position, changez
%         la position du deuxième
%       \item Sinon, si le premier levier est vers le bas, abbaissez le dernier
%       \item Sinon, si le deuxième levier est dans la même position que le premier
%         abbaissez le deuxième
%       \item Sinon, relever le dernier levier
%     \end{enumerate}
%   \end{center}
% \columnbreak
%   \begin{center}
%     Extrait du manuel orange : \\ \vspace{0.5cm}
%     \begin{enumerate}
%       \item Si le premier et le deuxième levier sont dans des positions différentes,
%         changez la position du dernier
%       \item Sinon, si le deuxième levier est vers le haut, abbaissez le premier
%       \item Sinon, si le troisième levier est dans la même position que le premier
%         relever le deuxième
%       \item Sinon, abaisser le dernier levier
%     \end{enumerate}
%   \end{center}
% \end{multicols}
% 
% \subsection*{Salle \croix}
% C'est une salle avec un panneau avec quatre boutons marqués de symboles. Il
% faudra appuyer sur les différents boutons dans un ordre précis.
% 
% Commencez par calculer la puissance de chaque bouton :
% 
% \begin{multicols}{2}
%   \begin{center}
%     Extrait du manuel vert :
%       \begin{itemize}
%         \item Si le symbole est un \carre, il vaut toujours 6 de puissance.
%         \item Si le symbole est \porte, il vaut 9 de puissance
%         \item Si le symbole est un \rond, il vaut 9 s'il est en haut, 8 à droite, 6 en bas,
%           5 à gauche.
%         \item Si le symbole est un \los, il vaut 1 de puissance de plus que le symbole
%           d'en face.
%         \item Si le symbole est un \croix, sa valeur est 7 s'il est en haut, le
%           double du symbole en face sinon.
%         \item Si le symbole est un \etoile, sa valeur est la somme des puissances
%           des deux symboles à côté moins celui d'en face.
%         \item Si le symbole est \tri, sa valeur est la moitié de la
%           puissance du symbole à droite, à laquelle on ajoute 3.
%       \end{itemize}
%   \end{center}
%   \columnbreak
%   \begin{center}
%     Extrait du manuel orange :
%     \begin{itemize}
%       \item Si le symbole est un \carre, il vaut toujours 4 de puissance.
%       \item Si le symbole est \porte, il vaut toujours 5 de puissance.
%       \item Si le symbole est un \rond, il vaut 6 puissance s'il est en bas, 12
%         sinon
%       \item Si le symbole est un \los, il vaut 7 s'il est en haut et 3 de moins que
%         celui d'en face sinon
%       \item Si le symbole est un \croix, sa valeur est 3
%       \item Si le symbole est un \etoile, sa valeur est la somme des puissances
%         des deux symboles à côté.
%       \item Si le symbole est un \tri, sa valeur est celle de celui d'en face auquel
%         on ajoute 1.
%     \end{itemize}
%   \end{center}
% \end{multicols}
% 
% Appuyez alors sur les deux boutons les plus puissants.
% 
% \subsection*{Salle \etoile}
% Lorsqu'elle est présente, c'est toujours la première énigme à résoudre. C'est
% une salle avec deux boutons, un avec un \los\ et un \tri. Il faudra appuyer
% plusieurs fois sur chacun d'entre eux dans le bon ordre.
% 
% Pour déterminer comment appuyer sur les boutons, regardez dans l'ordre le
% symbole de la salle au nord, puis au sud, puis à l'est, puis à l'ouest, et
% appuyez à chaque fois sur les boutons comme indiqué ci-dessous :
% 
% \begin{center}
% Extrait du manuel vert :
% \vspace{0.5cm}
%   \begin{tabular}{|c|c|c|c|c|c|c|c|}
%     \hline \rowcolor{Gray}
%     \NU{1}{er} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \los\tri\los & \los & \tri & \los\los & \los\tri \\ \hline
%     \hline \rowcolor{Gray}
%     \NU{2}{ème} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \tri\tri\los & \los & \tri\tri & \los & \los\los\tri \\ \hline
%     \hline \rowcolor{Gray}
%     \NU{3}{ème} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \tri & \los & \los\los & \los\tri\tri & \los\tri\los \\ \hline
%     \hline \rowcolor{Gray}
%     \NU{4}{ème} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \tri\los & \tri & \los & \los\los\tri & \tri\los\tri \\ \hline
%   \end{tabular}
% \end{center}
% 
% \begin{center}
% Extrait du manuel orange :
% \vspace{0.5cm}
%   \begin{tabular}{|c|c|c|c|c|c|c|c|}
%     \hline \rowcolor{Gray}
%     \NU{1}{er} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \tri & \los & \los\los & \los\tri\tri & \los\tri\los \\ \hline
%     \hline \rowcolor{Gray}
%     \NU{2}{ème} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \los\tri\los & \los & \tri & \los\los & \los\tri \\ \hline
%     \hline \rowcolor{Gray}
%     \NU{3}{ème} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \tri\los & \tri & \los & \los\los\tri & \tri\los\tri \\ \hline
%     \hline \rowcolor{Gray}
%     \NU{4}{ème} & \rond & \los & \croix & \etoile & \porte \\ \hline
%     Faites & \tri\tri\los & \los & \tri\tri & \los & \los\los\tri \\ \hline
%   \end{tabular}
% \end{center}
% 
