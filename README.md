# Projet d'informatique débranchée

Réalisé dans le cadre de l'UE informatique débranchée, sous la direction de
Fabien Taris[sang]

Le projet consiste en la présentation de concepts basiques de langages formels à
des classes de fin de primaires, début collège.


On retrouve dans le dossier doc la fiche de présentation de l'activité et le
guide à suivre en classe.

Le protocole de communication est présent dans le dossier protocole et les
manuels dans le dossier manuel. Une description des différents chateau ainsi
que les images rendues sont dans le dossier chateau.
