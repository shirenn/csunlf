#!/usr/bin/env python3
import subprocess
import yaml
from jinja2 import Template, FileSystemLoader, Environment

with open(r'data.yaml') as datas:
    file_loader = FileSystemLoader('./')
    env = Environment(loader=file_loader)
    datas = yaml.load(datas, Loader=yaml.FullLoader)
    for data in datas :
        template = env.get_template('template.tex.j2')
        file = open("rendu/"+data["fichier"]+".tex","w")
        file.write(template.render(data=data))
        file.close()

subprocess.call(['make','-C','rendu/'])
