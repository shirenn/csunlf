# Chateau Violet

## Étage 1

  - Chateau :
    - rond-losange
    - \><-losange-X

  - Réponse :
    - rond-triangle
    - \><-losange

  - Code :
    - #losange le message envoyé = 2
    - #rond dans le message reçu = 1
    - 7 si tu as pris la porte marqué d'un losange ; 8 si tu as pris la porte
      marqué d'un X = 7
