# Chateau vert
## Étage 1

  - Chateau :
    - rond-carré
    - \><-étoile-rond

  - Réponse :
    - rond-triangle
    - \><-étoile

  - Code:
    - #rond dans le message envoyé = 2
    - #étoile dans le message reçu = 1
    - 4 si tu as laissé le casque ; 6 sinon = 6

## Étage 2

  - Chateau :
    - rond-étoile
    - losange-etoile-X-étoile
    - X-carré-étoile-rond-><
    - \><-carre-X

  - Réponse :
    - rond-X
    - losange-><-><
    - X-><-étoile
    - \><-carré

  - Code :
    - #X dans le message envoyé = 2
    - #door dans le message recu = 4
    - #le nombre de levier abaissé après résolution des énigmes = 0

## Étage 3

  - Chateau :
    - rond-><
    - losange-X-X-étoile
    - X-triangle-X-losange-rond
    - étoile-losange-rond-><-X
    - \><-losange-triangle

  - Réponse:
    - étoile-losange-triangle-triangle-losange-losange-triangle-losange-losange
    - rond-triangle
    - losange-><-><-><
    - X-X-losange
    - \><-triangle

  - Code :
    - #rond dans le message envoyé = 3
    - #losange dans le message recu = 7
    - 3 si le denier bouton sur lequel vous appuyé dans la salle étoile est un
      losange, 6 si c'est un triangle

