# Chateau orange

## Étage 1

  - Chateau :
    - rond-étoile
    - \><-rond-X

  - Réponse :
    - rond-triangle
    - \><-rond

  - Code :
    - #X dans le message envoyé = 1
    - #rond dans le message reçu = 2
    - 3 si tu as pris la porte marqué d'un rond ; 5 si tu as pris la porte
      marqué d'un X = 3

## Étage 2

  - Chateau :
    - rond-><
    - losange-étoile-étoile-X
    - X-losange-X-triangle-><
    - \><-X-carré

  - Réponse :
    - rond-étoile
    - losange-><-><
    - X-losange-triangle
    - \><-X

  - Code :
    - #>< dans le message envoyé = 3
    - #X dans le message reçu = 2
    - #le nombre de levier relevé après la résolution des énigmes = 1

## Étage 3

  - Château :
    - rond-losange
    - losange-X-X-étoile
    - X-carré-rond-étoile-losange
    - étoile-losange-rond-><-X
    - \><-losange-triangle

  - Réponse :
    - étoile-losange-losange-triangle-losange-triangle-losange-triangle-triangle-triangle
    - rond-X
    - losange-><-><-><
    - X-étoile-losange
    - \><-losange

  - Code :
    - #losange dans le message envoyé = 5
    - #triangle dans le message reçu = 5
    - 2 si le dernier bouton sur lequel vous avez appuyé dans la salle étoile
      est un losange 5 si c'est un triangle = 5
